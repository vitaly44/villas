module.exports = function (gulp, plugins, path) {
	return () => {

		function getConn() {
			return plugins.ftp.create({
				host: 'villas.artbayard.ru',
				user: 'villas.artbayard.ru|ftp_villas',
				pass: '1234'
			});
		}

		var conn = getConn();

		return gulp.src(path.public.all)
			.pipe(conn.dest('./'));
	}
}