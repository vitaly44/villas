module.exports = function (gulp, plugins, path) {
	return () => {
		return gulp.src([path.src.pug])
			.pipe(plugins.plumber({
				errorHandler: plugins.notify.onError()
			}))
			.pipe(plugins.pug({
				pretty: true
			}))
			.pipe(plugins.typograf({
				locale: ['ru', 'en-US'],
				htmlEntity: {
					type: 'name'
				}
			}))
			.pipe(gulp.dest(path.public.root))
			.pipe(plugins.connect.reload());
	}
}