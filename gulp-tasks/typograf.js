module.exports = function (gulp, plugins, path) {
	return () => {
		return gulp.src(path.public.html + '*.html')
			.pipe(plugins.typograf({
				locale: ['ru', 'en-US'],
				htmlEntity: {
					type: 'name'
				}
			}))
			.pipe(gulp.dest(path.public.html));
	}
}