$(document).ready(function () {

    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 3,
                nav: false
            },
            1000: {
                items: 5,
                nav: true,
                loop: false
            }
        }
    })



    $('.section_bigimage--pool').viewportChecker({
        offset: '10%',
        invertBottomOffset: true,
        repeat: true, // Add the possibility to remove the class if the elements are not visible
        callbackFunction: function parallaxScroll() {
            var scrolled = $('.section_bigimage--pool').offset().top - $(window).scrollTop();
            $('.section_bigimage--pool_bg2').css('top', (0 - (scrolled * .25)) + 'px');
        }, // Callback to do after a class was added to an element. Action will return "add" or "remove", depending if the class was added or removed
    });

});